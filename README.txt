====================================
Make a tunnel:
====================================
openvpn --mktun --dev tun65

ip link set tun65 up

====================================
Give tunnel ip address
====================================
ip addr add 1.1.1.1 dev tun65
            └ tunnel ip
--or--
ip addr add 1.1.1.1 peer 159.203.250.248 dev tun65
            └ tunnel ip  |
                         └ Remote server ip


====================================
Route traffic to tunnel endpoint over wifi
====================================
ip route add 159.203.250.428 via 10.0.0.1 dev wlp3s0
             └ Remote server ip  |
                                 └ Gateway on wifi

====================================
Route all traffic through tunnel
====================================
ip route del default
ip route add default via 1.1.1.1 dev tun65
                         └ Address given to tunnel

====================================
Route all traffic back via wifi
====================================
ip route del default
ip route add default via 10.0.0.1 dev wlp3s0
                         └ Gateway on wifi
