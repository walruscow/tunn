use std::io::{Read, Write};
use std::net;
use std::thread;

use tun::open_tun;

pub fn read_tun(net: &mut Write, tun: &mut Read) {
    let mut buf = [0u8; 65540];
    loop {
        let len = match tun.read(&mut buf[4..]) {
            Ok(count) => count,
            Err(e) => {
                println!("Failed reading packet from tun: {}", e);
                return;
            },
        };

        println!("Read packet from tun with length {}", len);

        // Add the length in, most significant byte first
        for i in 0..4 {
            buf[i] = (len >> (24 - 8 * i)) as u8;
        }

        // Write length and packet to the network
        match net.write_all(&buf[..len + 4]) {
            Ok(_) => println!("Wrote {} bytes to net", len + 4),
            Err(e) => {
                println!("Failed writing to net: {}", e);
                return;
            },
        }
        net.flush().unwrap();
    }
}

pub fn read_net(net: &mut Read, tun: &mut Write) {
    let mut buf = [0u8; 65540];
    loop {
        match net.read_exact(&mut buf[..4]) {
            Ok(_) => (),
            Err(e) => {
                println!("Failed reading length from net: {}", e);
                return;
            },
        }

        let mut len = 0u32;
        // Read the most significant byte first
        for (i, b) in (&buf[..4]).iter().enumerate() {
            len |= (*b as u32) << (24 - i * 8);
        }
        println!("Read packet length from net {}", len);

        // Read packet
        match net.read_exact(&mut buf[..len as usize]) {
            Ok(_) => (),
            Err(e) => {
                println!("Failed reading packet from net: {}", e);
                return;
            },
        };
        println!("Read packet from net size {}", len);

        match tun.write_all(&buf[..len as usize]) {
            Ok(_) => println!("Wrote {} bytes to tun", len),
            Err(e) => {
                println!("Failed writing to tun: {}", e);
                return;
            },
        }
    }
}

pub fn run_client() {
    let mut tun = match unsafe { open_tun() } {
        Ok(fh) => fh,
        Err(_) => {
            println!("Error opening tun");
            return;
        },
    };

    let mut net = match net::TcpStream::connect("159.203.250.248:8080") {
        Ok(s) => s,
        Err(e) => {
            println!("Error opening tcp stream {}", e);
            return;
        },
    };

    let net_reader = {
        let mut net = net.try_clone().unwrap();
        let mut tun = tun.try_clone().unwrap();
        // Read the network and write to the tun
        thread::spawn(move || {
            read_net(&mut net, &mut tun);
        })
    };
    // Read the tun and write to the network
    let tun_reader = thread::spawn(move || {
        read_tun(&mut net, &mut tun);
    });

    match net_reader.join() {
        Ok(_) => println!("Net reader finished"),
        Err(_) => println!("Net reader failed"),
    }
    match tun_reader.join() {
        Ok(_) => println!("Tun reader finished"),
        Err(_) => println!("Tun reader failed"),
    }
    println!("Client finished.");
}
