extern crate libc;

mod client;
mod ip;
mod ping;
mod server;
mod socket;
mod tun;

use std::env;
use std::io::{Read, Write};

use ping::Ping;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Specify 'client' or 'server'");
        println!("Usage: {} {{ client | server }}", args[0]);
        return;
    }

    if args[1] == "client" {
        client::run_client();
    } else if args[1] == "server" {
        server::run_server();
    } else {
        println!("Specify 'client' or 'server'");
        println!("Usage: {} {{ client | server }}", args[0]);
        return;
    }
}

fn print_buf(buf: &[u8]) {
    let mut i = 0;
    for b in buf.iter() {
        print!("{:0>2x} ", *b);
        i += 1;
        if i == 8 {
            print!("  ");
        }
        if i == 16 {
            print!("\n");
            i = 0;
        }
    }
}
