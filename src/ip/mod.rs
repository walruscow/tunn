mod packet4;
mod header4;

pub mod v4 {
    use std::result;

    pub use std::net::Ipv4Addr as Address;
    pub use super::header4::Header;
    pub use super::packet4::Packet;

    #[derive(Debug)]
    pub enum PacketError {
        InvalidValue,
        TooShort,
        TooLong,
    }

    #[derive(Debug)]
    pub enum ValidationError {
        VersionInvalid,
        HeaderLengthInvalid,
        TotalLengthInvalid,
        FlagsInvalid,
        ChecksumInvalid,
        DestinationInvalid,
        SourceInvalid,
    }

    pub type PacketResult<T> = result::Result<T, PacketError>;
    pub type ValidationResult<T> = result::Result<T, ValidationError>;
}

/// Computes the Internet checksum for a buffer.
pub fn internet_checksum(header: &[u8]) -> u16 {
    let mut sum = 0u32;
    let mut word = 0u32;
    let mut flag = false;
    for b in header.iter() {
        word = (word << 8) | (*b as u32);
        if flag {
            flag = false;
            sum += word;
            word = 0;
        } else {
            flag = true;
        }
    }
    // If odd length
    if flag {
        sum += word;
    }

    // Reduce to 16 bits
    while sum > 0xFFFF {
        let carry = sum >> 16;
        sum = (sum & 0xFFFF) + carry;
    }
    !(sum as u16)
}
