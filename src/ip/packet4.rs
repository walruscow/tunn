use std::fmt;
use std::io;
use std::io::Write;
use std::u32;

use ip::v4::{PacketError, PacketResult, ValidationError, ValidationResult};
use ip::v4::Header;

pub struct Packet {
    pub header: Header,
    pub body: [u8; 65516],
    pub body_length: usize,
}

impl Packet {
    pub fn write_to_buf(&self, buf: &mut [u8]) -> usize {
        let header_length = match self.header.into_buf(buf) {
            Ok(header_length) => header_length,
            Err(_) => unreachable!(),
        };

        let mut buf = &mut buf[header_length..];
        let body_iter = self.body.iter().take(self.body_length);
        for (body_byte, buf_byte) in body_iter.zip(buf.iter_mut()) {
            *buf_byte = *body_byte;
        }

        header_length + self.body_length
    }

    pub fn write_to(&self, out: &mut Write) -> io::Result<usize> {
        let mut buf = [0u8; 65536];
        let bytes = self.write_to_buf(&mut buf);
        // TODO: Error if we didn't write the whole packet?
        out.write(&buf[..bytes])
    }

    pub fn write_to_with_length(&self, out: &mut Write) -> io::Result<usize> {
        let length = self.header.get_actual_length() + self.body_length;
        let length = length as u32;
        let mut buf = [0u8; 4];
        for i in 0..4 {
            buf[i] = (length >> (24 - 8 * i)) as u8;
        }
        match out.write(&buf) {
            Ok(count) => if count < 4 {
                return Ok(count);
            },
            Err(e) => return Err(e),
        }
        self.write_to(out)
    }

    pub fn create_from(buf: &[u8]) -> PacketResult<Packet> {
        let header = match Header::create_from(buf) {
            Ok(header) => header,
            Err(e) => return Err(e),
        };

        let bytes_used = (header.get_header_length() * 4) as usize;
        let buf = &buf[bytes_used..];
        if buf.len() > 65516 {
            return Err(PacketError::TooLong);
        }

        let mut body = [0u8; 65516];
        for (i, b) in buf.iter().enumerate() {
            body[i] = *b;
        };

        Ok(Packet {
            header: header,
            body: body,
            body_length: buf.len(),
        })
    }

    /// Modifies the packet to make it valid.
    ///
    /// This will overwrite the following fields in the IP header to make
    /// them valid:
    /// - version
    /// - header_length
    /// - total_length
    /// - checksum
    /// - flags
    pub fn fixup(&mut self) {
        self.header.set_version(4).unwrap();

        let header_length = self.header.get_actual_length();
        self.header.set_header_length((header_length / 4) as u8).unwrap();

        let total_len = header_length + self.body_length;
        self.header.set_total_length(total_len as u16).unwrap();

        self.header.set_extra_flag(false).unwrap();

        let checksum = self.header.compute_checksum();
        self.header.set_checksum(checksum).unwrap();
    }

    /// Determines if the packet is valid, or why not if it isn't.
    pub fn is_valid(&self) -> ValidationResult<()> {
        if self.header.get_version() != 4 {
            return Err(ValidationError::VersionInvalid);
        }

        let hlen_bytes = (self.header.get_header_length() as usize) * 4;
        if self.header.get_actual_length() != hlen_bytes {
            return Err(ValidationError::HeaderLengthInvalid);
        }

        let total_length = self.header.get_actual_length() + self.body_length;
        if total_length != (self.header.get_total_length() as usize) {
            return Err(ValidationError::TotalLengthInvalid);
        }

        if self.header.get_extra_flag() {
            return Err(ValidationError::FlagsInvalid);
        }

        if self.header.compute_checksum() != self.header.get_checksum() {
            println!("got the checksum {}", self.header.compute_checksum());
            println!("stored checksum {}", self.header.get_checksum());
            return Err(ValidationError::ChecksumInvalid);
        }

        if self.header.get_dest_ip().is_documentation() {
            return Err(ValidationError::DestinationInvalid);
        }

        if self.header.get_source_ip().is_documentation() {
            return Err(ValidationError::SourceInvalid);
        }

        Ok(())
    }

    pub fn new() -> Packet {
        Packet {
            header: Header::new(),
            body: [0; 65516],
            body_length: 0,
        }
    }

    pub fn set_body(&mut self, body: &[u8]) {
        self.body_length = body.len();
        (&mut self.body[..body.len()]).clone_from_slice(body);
    }
}

impl fmt::Display for Packet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = [0u8; 65536];
        let length = self.write_to_buf(&mut buf);
        try!(write!(
            f,
            "From: {} To: {}\n",
            self.header.get_source_ip(),
            self.header.get_dest_ip(),
        ));

        let mut i = 0;
        for b in (&buf[..length]).iter() {
            try!(write!(f, "{:0>2x} ", *b));
            i += 1;
            if i == 8 {
                try!(write!(f, "  "));
            }
            else if i == 16 {
                try!(write!(f, "\n"));
                i = 0;
            }
        }

        Ok(())
    }
}
