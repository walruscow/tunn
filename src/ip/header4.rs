use ip::internet_checksum;
use ip::v4::Address;
use ip::v4::{PacketError, PacketResult};

const EXTRA_FLAG: u8 = 0b100;
const DONT_FRAGMENT: u8 = 0b010;
const MORE_FRAGMENTS: u8 = 0b001;

#[derive(Debug)]
pub struct Header {
    version: u8,
    header_length: u8,
    dscp: u8,
    ecn: u8,
    total_length: u16,
    identification: u16,
    flags: u8,
    fragment_offset: u16,
    ttl: u8,
    protocol: u8,
    checksum: u16,
    source_ip: Address,
    dest_ip: Address,

    // TODO: Make an options struct or something
    //options: [u8; 40],
}

impl Header {
    pub fn get_version(&self) -> u8 { self.version }
    pub fn set_version(&mut self, version: u8) -> PacketResult<()> {
        if version > 0xF {
            return Err(PacketError::InvalidValue);
        }
        self.version = version;
        Ok(())
    }

    pub fn get_header_length(&self) -> u8 { self.header_length }
    pub fn set_header_length(&mut self, len: u8) -> PacketResult<()> {
        if len > 0xF {
            return Err(PacketError::InvalidValue);
        }
        self.header_length = len;
        Ok(())
    }

    pub fn get_dscp(&self) -> u8 { self.dscp }
    pub fn set_dscp(&mut self, dscp: u8) -> PacketResult<()> {
        if dscp > 0b111111 {
            return Err(PacketError::InvalidValue);
        }
        self.dscp = dscp;
        Ok(())
    }

    pub fn get_ecn(&self) -> u8 { self.ecn }
    pub fn set_ecn(&mut self, ecn: u8) -> PacketResult<()> {
        if ecn > 0b11 {
            return Err(PacketError::InvalidValue);
        }
        self.ecn = ecn;
        Ok(())
    }

    pub fn get_total_length(&self) -> u16 { self.total_length }
    pub fn set_total_length(&mut self, len: u16) -> PacketResult<()> {
        self.total_length = len;
        Ok(())
    }

    pub fn get_identification(&self) -> u16 { self.identification }
    pub fn set_identification(&mut self, ident: u16) -> PacketResult<()> {
        self.identification = ident;
        Ok(())
    }

    pub fn get_dont_fragment(&self) -> bool {
        (self.flags & DONT_FRAGMENT) != 0
    }
    pub fn set_dont_fragment(&mut self, flag: bool) -> PacketResult<()> {
        if flag {
            self.flags |= DONT_FRAGMENT;
        } else {
            self.flags &= !DONT_FRAGMENT;
        }
        Ok(())
    }

    pub fn get_more_fragments(&self) -> bool {
        (self.flags & MORE_FRAGMENTS) != 0
    }
    pub fn set_more_fragments(&mut self, flag: bool) -> PacketResult<()> {
        if flag {
            self.flags |= MORE_FRAGMENTS;
        } else {
            self.flags &= !MORE_FRAGMENTS;
        }
        Ok(())
    }

    pub fn get_extra_flag(&self) -> bool {
        (self.flags & EXTRA_FLAG) != 0
    }
    pub fn set_extra_flag(&mut self, flag: bool) -> PacketResult<()> {
        if flag {
            self.flags |= EXTRA_FLAG;
        } else {
            self.flags &= !EXTRA_FLAG;
        }
        Ok(())
    }

    pub fn get_fragment_offset(&self) -> u16 { self.fragment_offset }
    pub fn set_fragment_offset(&mut self, offset: u16) -> PacketResult<()> {
        if offset > 0x1FFF {
            return Err(PacketError::InvalidValue);
        }
        self.fragment_offset = offset;
        Ok(())
    }

    pub fn get_ttl(&self) -> u8 { self.ttl }
    pub fn set_ttl(&mut self, ttl: u8) -> PacketResult<()> {
        self.ttl = ttl;
        Ok(())
    }

    pub fn get_protocol(&self) -> u8 { self.protocol }
    pub fn set_protocol(&mut self, protocol: u8) -> PacketResult<()> {
        self.protocol = protocol;
        Ok(())
    }

    pub fn get_checksum(&self) -> u16 { self.checksum }
    pub fn set_checksum(&mut self, checksum: u16) -> PacketResult<()> {
        self.checksum = checksum;
        Ok(())
    }

    pub fn get_source_ip(&self) -> Address { self.source_ip }
    pub fn set_source_ip(&mut self, ip: Address) -> PacketResult<()> {
        self.source_ip = ip;
        Ok(())
    }

    pub fn get_dest_ip(&self) -> Address { self.dest_ip }
    pub fn set_dest_ip(&mut self, ip: Address) -> PacketResult<()> {
        self.dest_ip = ip;
        Ok(())
    }

    pub fn create_from(buf: &[u8]) -> PacketResult<Header> {
        if buf.len() < 20 {
            return Err(PacketError::TooShort);
        }

        let version = buf[0] >> 4;
        let header_length = buf[0] & 0xF;
        if buf.len() < (header_length * 4) as usize {
            return Err(PacketError::TooShort);
        }

        let to_u16 = |b: &[u8]| -> u16 {
            ((b[0] as u16) << 8) | (b[1] as u16)
        };

        let flags = buf[6] >> 5;

        Ok(Header {
            version: version,
            header_length: header_length,
            dscp: buf[1] >> 2,
            ecn: buf[1] & 0b11,
            total_length: to_u16(&buf[2..4]),
            identification: to_u16(&buf[4..6]),
            flags: buf[6] >> 5,
            fragment_offset: to_u16(&buf[6..8]) & 0x1FFF,
            ttl: buf[8],
            protocol: buf[9],
            checksum: to_u16(&buf[10..12]),
            source_ip: Address::new(buf[12], buf[13], buf[14], buf[15]),
            dest_ip: Address::new(buf[16], buf[17], buf[18], buf[19]),
        })
    }

    // The actual size used by this header (not what is recorded in the header)
    pub fn get_actual_length(&self) -> usize {
        20 + self.get_options_size()
    }

    fn get_options_size(&self) -> usize {
        0 // TODO
    }

    /// Write the header into the given buffer.
    ///
    /// Write the header into the buffer, as it should appear on the wire.
    /// Return the number of bytes used in the buffer, or an error if one
    /// occurred.
    pub fn into_buf(&self, buf: &mut [u8]) -> PacketResult<usize> {
        if buf.len() < self.get_actual_length() {
            return Err(PacketError::TooShort);
        }

        let from_u16 = |arr: &mut [u8], val: u16| {
            arr[0] = (val >> 8) as u8;
            arr[1] = val as u8;
        };

        buf[0] = (self.version) << 4;
        buf[0] |= self.header_length;
        buf[1] = self.dscp << 2;
        buf[1] |= self.ecn;
        from_u16(&mut buf[2..4], self.total_length);
        from_u16(&mut buf[4..6], self.identification);
        from_u16(&mut buf[6..8], self.fragment_offset);
        buf[6] |= self.flags << 5;
        buf[8] = self.ttl;
        buf[9] = self.protocol;
        from_u16(&mut buf[10..12], self.checksum);
        (&mut buf[12..16]).clone_from_slice(&self.source_ip.octets());
        (&mut buf[16..20]).clone_from_slice(&self.dest_ip.octets());

        Ok(self.get_actual_length())
    }

    pub fn compute_checksum(&self) -> u16 {
        let mut buf = [0u8; 60];
        let size = self.into_buf(&mut buf).unwrap();
        // Zero the checksum field
        buf[10] = 0;
        buf[11] = 0;
        internet_checksum(&buf[..size])
    }

    pub fn new() -> Header {
        Header {
            // Default values that make sense
            version: 4,
            header_length: 5,
            ttl: 64,

            source_ip: Address::new(0, 0, 0, 0),
            dest_ip: Address::new(0, 0, 0, 0),

            dscp: 0, ecn: 0, total_length: 0, identification: 0,
            fragment_offset: 0, flags: 0, protocol: 0, checksum: 0,
        }
    }
}
