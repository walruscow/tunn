use libc;

use std::fs::{File, OpenOptions};
use std::os::unix::io::AsRawFd;

const IFF_TUN: u16 = 1;
const IFF_NO_PI: u16 = 4096;
const TUNSETIFF: libc::c_ulong = 1074025674;

const TUN_FILE_NAME: &'static str = "/dev/net/tun";

enum TunType {
    TUN,
    TAP,
}

#[repr(C)]
struct TunIfReq {
    pub ifr_name: [u8; 16],
    pub flags: u16,
}

impl TunIfReq {
    pub fn new(tun_type: TunType) -> TunIfReq {
        let flag = match tun_type {
            TunType::TUN => IFF_TUN,
            TunType::TAP => 2,
        };
        let flag = flag | IFF_NO_PI;
        let name = {
            let mut name = [0u8; 16];
            // tun65 - name is arbitrary.
            for (i, x) in (&[116, 117, 110, 54, 53]).iter().enumerate() {
                name[i] = *x;
            }
            name
        };
        TunIfReq {
            ifr_name: name,
            flags: flag,
        }
    }
}

pub unsafe fn open_tun() -> Result<File, ()> {
    let res = OpenOptions::new().read(true).write(true).open(TUN_FILE_NAME);
    let file = match res {
        Ok(fh) => fh,
        Err(_) => {
            println!("Error opening /dev/net/tun");
            return Err(());
        },
    };

    let if_req = TunIfReq::new(TunType::TUN);
    let err = libc::ioctl(file.as_raw_fd(), TUNSETIFF, &if_req);
    if err < 0 {
        println!("Error ioctl");
        return Err(());
    }

    Ok(file)
}
