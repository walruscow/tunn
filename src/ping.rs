use std::io::{Write};

use ip;

pub struct Ping {
    dest: ip::v4::Address,
    source: ip::v4::Address,
}

impl Ping {
    pub fn new(src: ip::v4::Address, dest: ip::v4::Address) -> Ping {
        Ping {
            dest: dest,
            source: src,
        }
    }

    pub fn send<T: Write>(self, out: &mut T) -> Result<(), ()> {
        let mut ping_packet = ip::v4::Packet::new();
        ping_packet.header.set_dont_fragment(true).unwrap();
        ping_packet.header.set_protocol(1).unwrap();
        ping_packet.header.set_source_ip(self.source).unwrap();
        ping_packet.header.set_dest_ip(self.dest).unwrap();
        let mut icmp_body = [
            0x08, 0x00, 0x00, 0x00, 0x38, 0xa8, 0x00, 0x01,
            0xc8, 0x86, 0xcd, 0x57, 0x00, 0x00, 0x00, 0x00,
            // Data
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];

        let checksum_idx = 2;
        let ident_idx = checksum_idx + 2;
        let seq_idx = ident_idx + 2;
        let ts_idx = seq_idx + 2;
        let data_idx = ts_idx + 8;
        for (i, b) in icmp_body[data_idx..].iter_mut().enumerate() {
            *b = i as u8;
        }

        // Compute the checksum
        let checksum = ip::internet_checksum(&icmp_body);
        icmp_body[checksum_idx] = (checksum >> 8) as u8;
        icmp_body[checksum_idx + 1] = checksum as u8;

        ping_packet.set_body(&icmp_body);
        // Set the length and compute the checksum
        ping_packet.fixup();

        match ping_packet.write_to(out) {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        }
    }
}
