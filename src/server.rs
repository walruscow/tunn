use std::io::{Read, Write};
use std::net;
use std::thread;

use ip;
use socket::{EthSocket, IpSocket};

/// Read packets from sent from the client and retransmit them.
fn read_client_packets(
    net: &mut Read,
    ip_socket: &mut IpSocket,
    our_ip: ip::v4::Address,
) {
    let mut buf = [0u8; 65540];
    loop {
        match net.read_exact(&mut buf[..4]) {
            Ok(_) => (),
            Err(e) => {
                println!("Failed reading length from net: {}", e);
                return;
            },
        };

        let mut len = 0u32;
        // Read the most significant byte first
        for (i, b) in (&buf[..4]).iter().enumerate() {
            len |= (*b as u32) << (24 - i * 8);
        }
        let len = len as usize;
        println!("Read packet from net with length {}", len);

        // Read packet.
        match net.read_exact(&mut buf[..len]) {
            Ok(_) => (),
            Err(e) => {
                println!("Failed reading packet from net: {}", e);
                return;
            },
        };

        let mut packet = match ip::v4::Packet::create_from(&buf[..len]) {
            Ok(p) => p,
            Err(_) => continue,
        };

        // We want to get the reply
        packet.header.set_source_ip(our_ip).unwrap();
        // Re-set the checksum
        packet.fixup();

        let dest_ip = packet.header.get_dest_ip();
        match packet.write_to(&mut ip_socket.to(dest_ip)) {
            Ok(b) => println!("Wrote {} bytes of packet to socket", b),
            // lol just drop it
            Err(_) => continue,
        };
    }
}

fn is_possibly_ipv4(buf: &[u8]) -> bool {
    // Check if the ipv4 flag is there
    (buf[0] >> 4) == 4
}

/// Read packets and send appropriate ones back to the client
fn read_incoming_packets(
    eth_socket: &mut Read,
    net: &mut Write,
    our_ip: ip::v4::Address,
    client_ip: ip::v4::Address,
) {
    // We are going to read *every* ethernet frame that passes this computer.
    // We need to filter down to the ones that are probably related
    let mut buf = [0u8; 65540];
    loop {
        let len = match eth_socket.read(&mut buf[4..]) {
            Ok(count) => count,
            Err(e) => {
                println!("Failed reading packet from ethernet: {}", e);
                return;
            },
        };

        if !is_possibly_ipv4(&buf[4..len + 4]) {
            continue;
        }

        let mut packet = match ip::v4::Packet::create_from(&buf[4..len + 4]) {
            Ok(p) => p,
            Err(_) => {
                println!("Could not create packet");
                continue;
            },
        };

        if packet.header.get_dest_ip() != our_ip {
            // This packet is not for us.
            println!("Not us");
            continue;
        }
        if packet.header.get_source_ip() == client_ip {
            // This is probably our tcp connection with the client.
            continue;
        }
        if packet.header.get_source_ip() == our_ip {
            // This is some normal data we're sending out.
            continue;
        }
        println!("Got packet from ethernet!");
        // At this point we've filtered it down to traffic destined for us,
        // not sent by the client and not sent by us.
        // We could do additional filtering now against IPs that the client
        // recently connected to, if we wanted.
        // ... Or we could send it all to the client and let it reject what it
        // wants to (e.g. the OS can, after tun insertion).
        packet.header.set_dest_ip(ip::v4::Address::new(10, 0, 0, 101)).unwrap();
        packet.fixup();
        match packet.write_to_with_length(net) {
            Ok(b) => println!("Sending {} bytes back to client", b),
            Err(_) => println!("Error sending back to client"),
        };
    }

}

fn handle_client(mut net: net::TcpStream) {
    let client_ip = match net.peer_addr() {
        Ok(sock_addr) => match sock_addr.ip() {
            net::IpAddr::V4(addr) => addr,
            net::IpAddr::V6(_) => return,
        },
        Err(_) => return,
    };
    let our_ip = match net.local_addr() {
        Ok(sock_addr) => match sock_addr.ip() {
            net::IpAddr::V4(addr) => addr,
            net::IpAddr::V6(_) => return,
        },
        Err(_) => return,
    };

    let mut eth_socket = match EthSocket::new() {
        Ok(socket) => socket,
        Err(_) => return,
    };
    let mut ip_socket = match IpSocket::new() {
        Ok(socket) => socket,
        Err(_) => return,
    };

    let outgoing_packets = {
        let mut net = net.try_clone().unwrap();
        thread::spawn(move || {
            read_client_packets(&mut net, &mut ip_socket, our_ip);
        })
    };
    let incoming_packets = thread::spawn(move || {
        read_incoming_packets(&mut eth_socket, &mut net, our_ip, client_ip);
    });

    match outgoing_packets.join() {
        Ok(_) => println!("Net reader finished"),
        Err(_) => println!("Net reader failed"),
    }
    match incoming_packets.join() {
        Ok(_) => println!("Tun reader finished"),
        Err(_) => println!("Tun reader failed"),
    }
    println!("Client finished.");
}

pub fn run_server() {
    let listener = net::TcpListener::bind("159.203.250.248:8080").unwrap();
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                // One at a time :)
                handle_client(stream);
            },
            Err(e) => {
                println!("Connection failed {}", e);
            },
        }
    }
}
