use std::io;
use std::io::{Read, Write};
use std::mem;
use std::ptr;

use libc;

use ip;

type CSocket = libc::c_int;
type CBuf = *const libc::c_void;
type MutCBuf = *mut libc::c_void;
type CSockAddr = *const libc::sockaddr;
type MutCSockAddr = *mut libc::sockaddr;
type CSockLen = libc::socklen_t;

fn make_libc_addr(ipv4_addr: ip::v4::Address) -> libc::sockaddr_in {
    let mut addr_int = 0u32;
    for (i, b) in ipv4_addr.octets().iter().enumerate() {
        addr_int |= (*b as u32) << 8 * i;
    }

    libc::sockaddr_in {
        sin_family: libc::AF_INET as libc::sa_family_t,
        sin_port: 0,            // Don't use the port
        sin_addr: libc::in_addr {
            s_addr: addr_int,   // Actual IPv4 address
        },
        sin_zero: [0u8; 8],     // Unused zero bytes
    }
}

pub struct IpSocket {
    socket: CSocket,
}

impl IpSocket {
    pub fn new() -> Result<IpSocket, io::Error> {
        let fd = unsafe {
            libc::socket(libc::AF_INET, libc::SOCK_RAW, libc::IPPROTO_RAW)
        };

        if fd < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(IpSocket {
                socket: fd,
            })
        }
    }

    pub fn to(&self, dest_ip: ip::v4::Address) -> IpSocketWriter {
        IpSocketWriter {
            dest_ip: dest_ip,
            socket_fd: self.socket,
        }
    }
}

pub struct IpSocketWriter {
    dest_ip: ip::v4::Address,
    socket_fd: CSocket,
}

impl Write for IpSocketWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let addr = make_libc_addr(self.dest_ip);
        let sent_bytes = unsafe {
            libc::sendto(
                self.socket_fd,
                buf.as_ptr() as CBuf,
                buf.len(),
                0, // Flags
                (&addr as *const libc::sockaddr_in) as CSockAddr,
                mem::size_of::<libc::sockaddr>() as CSockLen,
            )
        };

        if sent_bytes < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(sent_bytes as usize)
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

pub struct EthSocket {
    socket: CSocket,
}

impl EthSocket {
    pub fn new() -> Result<EthSocket, io::Error> {
        let fd = unsafe {
            libc::socket(libc::AF_PACKET, libc::SOCK_DGRAM, 768)
        };

        if fd < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(EthSocket {
                socket: fd,
            })
        }
    }
}

impl Read for EthSocket {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let read_bytes = unsafe {
            libc::recvfrom(
                self.socket,
                buf.as_mut_ptr() as MutCBuf,
                buf.len(),
                0, // Flags
                ptr::null_mut() as MutCSockAddr, // Don't need source address
                ptr::null_mut() as *mut CSockLen,
            )
        };

        if read_bytes < 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(read_bytes as usize)
        }
    }
}
