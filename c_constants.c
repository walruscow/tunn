#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <errno.h>
#include <arpa/inet.h>

int main(void) {
  printf("TUNSETIFF %lu\n", TUNSETIFF);
  printf("IFF_TUN %d\n", IFF_TUN);
  printf("IFF_NO_PI %d\n", IFF_NO_PI);
  printf("htons(ETH_P_ALL) %d\n", htons(ETH_P_ALL));
  printf("ETH_P_ALL %d\n", ETH_P_ALL);
}
